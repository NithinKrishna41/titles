﻿$(document).ready(function () {
    Initialize();
})
function Initialize() {
    $("#TitleTextBox").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Title/TitleList",
                dataType: "json",
                data: {
                    enteredValue: request.term
                },
                success: function (data) {
                    response(data.titleList);
                }
            });
        },
        select: function (event, ui) {
           window.location.href = "/Title/Detail/?enteredValue="+ui.item.value;
        }
    });
}
