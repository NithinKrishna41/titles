﻿using System.Linq;
using System.Web.Mvc;
using Titles.Repository;

namespace Titles.Controllers
{
    public class TitleController : Controller
    {
        private TitlesModelContainer m_Container = new TitlesModelContainer();

        /// <summary>
        /// This action method returns the search view
        /// which is used to search the titles
        /// </summary>
        /// <returns>Returns the search view</returns>
        public ActionResult Search()
        {
            return View();
        }

        /// <summary>
        /// This action method is invoked, the moment, the user types in a key
        /// in the search text box.
        /// </summary>
        /// <param name="enteredValue">The value on which user has to search</param>
        /// <returns>It returns list of titles that contains the typed in word</returns>
        public JsonResult TitleList(string enteredValue)
        {
            var titleList = m_Container.Titles.Where(x => x.TitleName.Contains(enteredValue)).Select(x => x.TitleName).ToList();
            return Json(new { titleList = titleList }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This action is invoked when the user has selected a title from the list displayed
        /// in the search box
        /// </summary>
        /// <param name="enteredValue">The title choosen by the user.</param>
        /// <returns>Returns the details view</returns>
        public ActionResult Detail(string enteredValue)
        {
            var titleInformation = m_Container.Titles.Where(x => x.TitleName == enteredValue).Single();
            return View(titleInformation);
        }
    }
}
